package org.tahomarobotics.robot.drivetrain;

import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkMaxLowLevel;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import org.tahomarobotics.robot.RobotMap;

public class Drivetrain extends SubsystemBase {

    private static final Drivetrain INSTANCE = new Drivetrain();

    private final CANSparkMax drivetrainMotorOneFront = new CANSparkMax(RobotMap.DRIVETRAIN_MOTOR_ONE_FRONT, CANSparkMaxLowLevel.MotorType.kBrushless);
    private final CANSparkMax drivetrainMotorOneBack = new CANSparkMax(RobotMap.DRIVETRAIN_MOTOR_ONE_BACK, CANSparkMaxLowLevel.MotorType.kBrushless);
    private final CANSparkMax drivetrainMotorTwoFront = new CANSparkMax(RobotMap.DRIVETRAIN_MOTOR_TWO_FRONT, CANSparkMaxLowLevel.MotorType.kBrushless);
    private final CANSparkMax drivetrainMotorTwoBack = new CANSparkMax(RobotMap.DRIVETRAIN_MOTOR_TWO_BACK, CANSparkMaxLowLevel.MotorType.kBrushless);

    public static Drivetrain getInstance() {
        return INSTANCE;
    }

    private Drivetrain() {
        drivetrainMotorOneFront.setInverted(false);
        drivetrainMotorOneBack.setInverted(false);
        drivetrainMotorTwoFront.setInverted(true);
        drivetrainMotorTwoBack.setInverted(true);

        drivetrainMotorOneFront.follow(CANSparkMax.ExternalFollower.kFollowerDisabled, 0);
        drivetrainMotorTwoFront.follow(CANSparkMax.ExternalFollower.kFollowerDisabled, 0);
        drivetrainMotorOneBack.follow(drivetrainMotorOneFront);
//        drivetrainMotorTwoFront.setInverted(false);
        drivetrainMotorTwoBack.follow(drivetrainMotorTwoFront);
    }

    @Override
    public void periodic() {

    }

    public void setDrivetrainMotorOneFront(double power) {
        drivetrainMotorOneFront.set(power);
    }

    public void setDrivetrainMotorOneBack(double power) {
        drivetrainMotorOneBack.set(power);
    }

    public void setDrivetrainMotorTwoFront(double power) {
        drivetrainMotorTwoFront.set(power);
    }

    public void setDrivetrainMotorTwoBack(double power) {
        drivetrainMotorTwoBack.set(power);
    }
}

package org.tahomarobotics.robot.drivetrain;

import edu.wpi.first.wpilibj2.command.CommandBase;
import org.tahomarobotics.robot.oi.OI;

public class DrivetrainCommand extends CommandBase {

    private final Drivetrain drivetrain = Drivetrain.getInstance();
    private final OI oi = OI.getInstance();

    public DrivetrainCommand() {
        addRequirements(drivetrain);
    }

    @Override
    public void execute() {
        drivetrain.setDrivetrainMotorOneFront(oi.getDriveRightStick() * 0.2);
//        drivetrain.setDrivetrainMotorOneFront(0.2);
//        drivetrain.setDrivetrainMotorOneBack(oi.getDriveLeftTrigger());
        drivetrain.setDrivetrainMotorTwoFront(oi.getDriveLeftStick() * 0.2);
//        drivetrain.setDrivetrainMotorTwoFront(0.2);
//        drivetrain.setDrivetrainMotorTwoBack(oi.getDriveRightTrigger());
    }

    @Override
    public void end(boolean interrupted) {
        drivetrain.setDrivetrainMotorOneFront(0);
        drivetrain.setDrivetrainMotorOneBack(0);
        drivetrain.setDrivetrainMotorTwoFront(0);
        drivetrain.setDrivetrainMotorTwoBack(0);
    }
}

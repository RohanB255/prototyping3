package org.tahomarobotics.robot.robotsolenoid;

import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import org.tahomarobotics.robot.RobotMap;

public class RobotSolenoid extends SubsystemBase {

//    private static final RobotSolenoid INSTANCE = new RobotSolenoid();

    private static final DoubleSolenoid doubleSolenoid_1 = new DoubleSolenoid(RobotMap.SOLENOID_ONE[0], RobotMap.SOLENOID_ONE[1]);
    private static final DoubleSolenoid doubleSolenoid_2 = new DoubleSolenoid(RobotMap.SOLENOID_TWO[0], RobotMap.SOLENOID_TWO[1]);

    private static final RobotSolenoid INSTANCE = new RobotSolenoid();

    public static RobotSolenoid getInstance() {
        return INSTANCE;
    }

    private RobotSolenoid() {
        doubleSolenoid_1.set(DoubleSolenoid.Value.kReverse);
        doubleSolenoid_2.set(DoubleSolenoid.Value.kReverse);
    }

    @Override
    public void periodic() {}

    public void toggleDoubleSolenoid_1() {
        doubleSolenoid_1.toggle();
    }

    public void toggleDoubleSolenoid_2() {
        doubleSolenoid_2.toggle();
    }

    public void killSolenoids() {
        doubleSolenoid_1.set(DoubleSolenoid.Value.kReverse);
        doubleSolenoid_2.set(DoubleSolenoid.Value.kReverse);
        doubleSolenoid_1.set(DoubleSolenoid.Value.kOff);
        doubleSolenoid_2.set(DoubleSolenoid.Value.kOff);
    }
}

package org.tahomarobotics.robot.robotsolenoid;

import edu.wpi.first.wpilibj.Solenoid;
import edu.wpi.first.wpilibj2.command.CommandBase;
import org.tahomarobotics.robot.Robot;
import org.tahomarobotics.robot.oi.OI;

public class SolenoidCommand extends CommandBase {

    private final RobotSolenoid solenoids = RobotSolenoid.getInstance();
    private final OI oi = OI.getInstance();

    public SolenoidCommand() {
        addRequirements(solenoids);
    }

    @Override
    public void execute() {
        if (oi.getDriveButtonA()) {
            solenoids.toggleDoubleSolenoid_1();
        }
        if (oi.getDriveButtonB()) {
            solenoids.toggleDoubleSolenoid_2();
        }
    }

    @Override
    public void end(boolean interrupted) {
        solenoids.killSolenoids();
    }
}

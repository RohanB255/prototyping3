package org.tahomarobotics.robot.chassis;

import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkMaxLowLevel;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import org.tahomarobotics.robot.RobotMap;

public class Chassis extends SubsystemBase {

    private static final Chassis INSTANCE = new Chassis();

    private final CANSparkMax chassisMotorOne = new CANSparkMax(RobotMap.CHASSIS_MOTOR_ONE, CANSparkMaxLowLevel.MotorType.kBrushless);
    private final CANSparkMax chassisMotorTwo = new CANSparkMax(RobotMap.CHASSIS_MOTOR_TWO, CANSparkMaxLowLevel.MotorType.kBrushless);

    public static Chassis getInstance() {
        return INSTANCE;
    }

    private Chassis() {
        chassisMotorOne.setInverted(false);
        chassisMotorTwo.setInverted(false);
    }

    @Override
    public void periodic() {

    }

    public void setChassisMotorOne(double power) {
        chassisMotorOne.set(power);
    }

    public void setChassisMotorTwo(double power) {
        chassisMotorTwo.set(power);
    }

}

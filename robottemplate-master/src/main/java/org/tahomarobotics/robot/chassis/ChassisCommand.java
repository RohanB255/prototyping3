package org.tahomarobotics.robot.chassis;

import edu.wpi.first.wpilibj2.command.CommandBase;
import org.tahomarobotics.robot.oi.OI;

public class ChassisCommand extends CommandBase {

    private final Chassis chassis = Chassis.getInstance();
    private final OI oi = OI.getInstance();

    public ChassisCommand() {
        addRequirements(chassis);
    }

    @Override
    public void execute() {
        chassis.setChassisMotorOne(oi.getDriveRightStick());
        chassis.setChassisMotorTwo(oi.getDriveLeftStick());
    }

    @Override
    public void end(boolean interrupted) {
        chassis.setChassisMotorOne(0);
        chassis.setChassisMotorTwo(0);
    }
}

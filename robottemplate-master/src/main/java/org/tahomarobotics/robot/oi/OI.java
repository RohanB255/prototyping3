package org.tahomarobotics.robot.oi;

import edu.wpi.first.wpilibj.GenericHID;
import edu.wpi.first.wpilibj.XboxController;

public class OI {

    private static final OI INSTANCE = new OI();
    private static final double DEADBAND = 5d /127;

    public static OI getInstance () {
        return INSTANCE;
    }
    private XboxController driveController = new XboxController(0);

    private OI() {}

    public double getDriveRightStick() {
        return calcDeadband(driveController.getY(GenericHID.Hand.kRight));
    }

    public double getDriveLeftStick() {
        return calcDeadband(driveController.getX(GenericHID.Hand.kLeft));
    }

    public double getDriveRightTrigger() {
        return driveController.getTriggerAxis(GenericHID.Hand.kLeft);
    }

    public double getDriveLeftTrigger() {
        return driveController.getTriggerAxis(GenericHID.Hand.kLeft);
    }

    public boolean getDriveButtonA() {
        return driveController.getAButtonPressed();
    }

    public boolean getDriveButtonB() {
        return driveController.getBButtonPressed();
    }

    private double calcDeadband(double input) {
        if(Math.abs(input) > DEADBAND) return input;
        return 0;
    }
}

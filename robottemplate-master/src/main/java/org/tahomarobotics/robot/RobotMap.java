package org.tahomarobotics.robot;

public class RobotMap {

    //Drivetrain
    public static final int DRIVETRAIN_MOTOR_ONE_FRONT = 1;
    public static final int DRIVETRAIN_MOTOR_ONE_BACK = 2;
    public static final int DRIVETRAIN_MOTOR_TWO_FRONT = 3;
    public static final int DRIVETRAIN_MOTOR_TWO_BACK = 4;
    public static final int CHASSIS_MOTOR_ONE = 1;
    public static final int CHASSIS_MOTOR_TWO = 2;

    //Solenoids
    public static final int[] SOLENOID_ONE = {0, 3};
    public static final int[] SOLENOID_TWO = {1, 2};
}
